# Odoo Italian Localized ISO Project 

The script provide an automated way to create a customized live ISO with Odoo preinstalled using ITA accounting localization. 

```
Usage: ./build_odoo_ita.sh [options]... DISTRO

Options:
  -h, --help
    Display this usage message and exit.

  -p <dir>, --project-dir <dir>, --project-dir=<dir>
    Set the main dir of the project.
    Default value is ./iso_build

  -i <dir>, --iso-dir <dir>, --iso-dir=<dir>
    Set the source ISO image dir.
    Default value is ./iso_build

  -d <dir>, --dest-dir <dir>, --dest-dir=<dir>
    Set the customized ISO image dir.
    Default value is ./iso_build

ODOO RELEASE
  -r <version>, --release <version>
    Set the Odoo release version to build.
    Supported values are:
      12 -> for Odoo v.12.0
      14 -> for Odoo v.14.0

    Default value is 14

DISTRO RELEASE
  Mandatory, this is release name to use as source for
  the custom ISO (64bit).

  Supported values are:
    ubuntu
        18.04.6 LTS for Odoo v.12.0
        20.04.5 LTS for Odoo v.14.0
    debian
        10.13 for Odoo v.12.0
        11.5  for Odoo v.14.0
```
