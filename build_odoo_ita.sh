#!/bin/bash
# Copyright 2017-2022 Sergio Zanchetta <primes2h@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

scriptversion="5.1"

usage() {
    cat <<EOF
Usage: $0 [options]... DISTRO

Options:
  -h, --help
    Display this usage message and exit.

  -p <dir>, --project-dir <dir>, --project-dir=<dir>
    Set the main dir of the project.
    Default value is $defaultdir

  -i <dir>, --iso-dir <dir>, --iso-dir=<dir>
    Set the source ISO image dir.
    Default value is $isodefaultdir

  -d <dir>, --dest-dir <dir>, --dest-dir=<dir>
    Set the customized ISO image dir.
    Default value is $defaultdir

ODOO RELEASE
  -r <version>, --release <version>
    Set the Odoo release version to build.
    Supported values are:
      12 -> for Odoo v.12.0
      14 -> for Odoo v.14.0

    Default value is $odoodefaultrel

DISTRO RELEASE
  Mandatory, this is release name to use as source for
  the custom ISO (64bit).

  Supported values are:
    ubuntu
        18.04.6 LTS for Odoo v.12.0
        20.04.5 LTS for Odoo v.14.0
    debian
        10.13 for Odoo v.12.0
        11.5  for Odoo v.14.0

EOF
}

# Logging and error handling functions
log() { printf '%s\n\n' "$*"; }
error() { log "ERROR: $*" >&2; }
fatal() { error "$*"; exit 1; }
usage_fatal() { error "$*"; usage >&2; exit 1; }

# Supported distro releases
bionic="ubuntu-18.04.6-desktop-amd64"
focal="ubuntu-20.04.5-desktop-amd64"
buster="debian-live-10.13.0-amd64-gnome"
bullseye="debian-live-11.5.0-amd64-gnome"

# Supported odoo releases
odooreleases=("12 14")

odoodefaultrel="14"
defaultdir="$PWD/iso_build"
isodefaultdir="$defaultdir/iso_source"
while [ "$#" -gt 0 ]; do
    arg=$1
    case $1 in
        # Convert "--opt=value" to --opt "value"
        # Quotes around equals sign is a workaround
        # for a bug in emacs' syntax parsing
        --*'='*) shift; set -- "${arg%%=*}" "${arg#*=}" "$@"; continue;;
        -p|--project-dir) shift; projectdir=$1;;
        -i|--iso-dir) shift; isodir=$1;;
        -d|--dest-dir) shift; destdir=$1;;
        -r|--release) shift; odoorel=$1;;
        -h|--help) usage; exit 0;;
        -*) usage_fatal "unknown option: '$1'";;
        *) break;; # Reached the list of file names
    esac
    shift || usage_fatal "option '${arg}' requires a value"
done

[ -z "$projectdir" ] && projectdir=$defaultdir
[ -z "$isodir" ] && isodir="$isodefaultdir"
[ -z "$destdir" ] && destdir="$projectdir"
[ -z "$odoorel" ] && odoorel="$odoodefaultrel"


# ISO version numbering:
year=$(date -d "$D" '+%y')
month=$(date -d "$D" '+%m')
isoversion=$((odoorel/2))".$year.$month"
echo $isoversion

distrorelname=("${!bionic@}|${!focal@}|${!buster@}|${!bullseye@}")
distro=$1

if [[ -z "$odoorel" ]]
then
    usage; exit 0
elif [[ ! " ${odooreleases[*]} " =~ " ${odoorel} " ]]
then
    usage_fatal "unknown odoo release '$odoorel'"
fi

case "$distro" in
ubuntu)
    [[ $odoorel == $odoodefaultrel ]] && distrorel="focal" || distrorel="bionic"
    relnumb="$(echo ${!distrorel} | cut -d - -f 2)"
    fsdir="casper"
    fsext="manifest"
    ;;
debian)
    [[ $odoorel == $odoodefaultrel ]] && distrorel="bullseye" || distrorel="buster"
    relnumb="$(echo ${!distrorel} | cut -d - -f 3)"
    [[ $distrorel == "bullseye" ]] && dtype="release" || dtype="archive"
    fsdir="live"
    fsext="packages"
    ;;
esac

if [[ ! "$distrorel" =~ ^($distrorelname)$ ]]
then
    usage_fatal "unknown distribution: '$distro'"
fi


# Create logfile
logfile="$projectdir/logfile-$distrorel.log" && exec > >(tee $logfile) 2>&1

# Project variables
PKGS="squashfs-tools genisoimage rsync"

services=("odoo postgres")

mntdir="mnt"
extractdir="extract"
editdir="edit"

customiso="odoo_ita-$(echo $isoversion)~$distrorel.iso"

user=$(whoami)

#clear

echo -e "=== Odoo Italian Localized ISO Project ===\n"

# Setting up project directories & variables
releasedir="$projectdir/$distrorel"

mkdir -p "$projectdir"
mkdir -p "$isodir"
mkdir -p "$destdir"

mntdir="$releasedir/$mntdir"
extractdir="$releasedir/$extractdir"
editdir="$releasedir/$editdir"

echo -ne "\n"

# Main cycle
while true
do
    PS3="Please select: "
    options=("Setup environment" "Customize ISO" "Create ISO" "Reset environment")
    select opt in "${options[@]}" "Quit"; do
        case "$REPLY" in

        # Setup environment
        1 ) echo -ne "\n"
            isoname=${!distrorel}.iso
            isoimage="$isodir/$isoname"
            if [ -f "$isoimage" ]
            then
                echo -e "Found ISO image. $isoname\n"
            else
                case "$distro" in
                ubuntu)
                    wget -P "$isodir" http://releases.ubuntu.com/$distrorel/$isoname || exit 1
                    ;;
                debian)
                    wget -P "$isodir" http://cdimage.debian.org/cdimage/$dtype/$relnumb"-live"/amd64/iso-hybrid/$isoname || exit 1
                    ;;
                esac
            fi

            mkdir -p "$releasedir"

            echo -ne "Checking for needed packages........."
            dpkg-query -W -f='${Status}' $PKGS 2>/dev/null | grep -q -P '^install ok installed$'
            if [ $? -eq 0 ]
            then
                echo -e "not installed"
                echo -ne "Installing packages........."
                sudo apt install $PKGS;
                echo -e "DONE\n"
            else
                echo -e "Packages already installed\n"
            fi

            if [ ! -d "$editdir" ] 
            then
                mkdir -p "$mntdir" || exit 1
                sudo mount -o loop "$isoimage" "$mntdir" || exit 1

                mkdir -p "$extractdir" || exit 1
                sudo rsync --exclude=/$fsdir/filesystem.squashfs -a $mntdir/ $extractdir || exit 1

                sudo unsquashfs -d $releasedir/squashfs-root $mntdir/$fsdir/filesystem.squashfs || exit 1
                sudo mv $releasedir/squashfs-root $editdir || exit 1

                sudo umount "$mntdir" || exit 1

            fi
            break
            ;;

        # Customize ISO
        2 ) [ ! -d "$editdir" ] && error "Environment is not set. Please select option 1) first." && break
            for service in $services
            do
                echo -ne "Check if $service service is running......"

                if [ $(pgrep -x -c $service) -gt 0 ]
                then
                    [ $service == "postgres" ] && service="postgresql"
                    echo -e "YES, stopping it. "
                    sudo service $service stop
                    echo -e "DONE\n"
                else
                    echo -e "NO, exiting\n"
                fi
            done      

            ## Copying initial setting files 
            sudo cp /etc/resolv.conf $editdir/etc/ || exit 1
            sudo sh -c "echo 'LC_ALL=it_IT.UTF-8' >> $editdir/etc/default/locale"

            ### Launcher and installer
            sudo cp ./files/odoo_ita.desktop $editdir/usr/share/applications/
	    sudo sed -i "s/<version>/$isoversion/g" $editdir/usr/share/applications/odoo_ita.desktop

            sudo cp ./files/odoo_pnlug-128x128.png $editdir/usr/share/icons/
	    if [[ ${distro} == "ubuntu" ]]
            then
                sudo cp ./files/ubiquity.desktop $editdir/usr/share/applications/
	        sudo sed -i "s/<odoo_release>/$odoorel/g" $editdir/usr/share/applications/ubiquity.desktop
                sudo cp ./files/install-to-disk.svg $editdir/usr/share/icons/
		sudo sed -i "s/Prompt=lts/Prompt=never/g" $editdir/etc/update-manager/release-upgrades
            fi
	    if [[ ${distro} == "debian" ]]
            then
                sudo cp ./files/install-debian.desktop $editdir/usr/share/applications/
	        sudo sed -i "s/<odoo_release>/$odoorel/g" $editdir/usr/share/applications/install-debian.desktop
            fi

            ### Odoo systemd service
            sudo cp ./files/odoo.service $editdir/lib/systemd/system/odoo$odoorel.service
	    sudo sed -i "s/<odoo_release>/$odoorel/g" $editdir/lib/systemd/system/odoo$odoorel.service

            ### odoo user bash prompt
            sudo cp ./files/{profile,bashrc} $editdir/root/

            ### Wallpaper
            sudo cp ./files/pnlug-odooitalia-oca_$distro.png $editdir/usr/share/backgrounds/
            sudo cp ./files/99_odoo-settings.gschema.override~$distrorel $editdir/usr/share/glib-2.0/schemas/99_odoo-settings.gschema.override
            sudo cp ./files/$distro-wallpapers.xml $editdir/usr/share/gnome-background-properties/


            echo -e "Binding dev......" 
            sudo mount --bind /dev $editdir/dev || exit 1

            echo -e "Enabling chroot......"
            sudo chroot $editdir /bin/bash <<'EOF'

            setup_chroot()
            {
                # Setup chroot environment
                mount -t proc none /proc
                mount -t sysfs none /sys
                mount -t devpts none /dev/pts
                export HOME=/root
                dbus-uuidgen > /var/lib/dbus/machine-id
                dpkg-divert --local --rename --add /sbin/initctl
                ln -s /bin/true /sbin/initctl
            }
    
            clean_chroot()
            {
                # Clean up system
                case "$distribution" in
                ubuntu)
                    apt autoremove && apt autoclean
                    ;;
                debian)
                    apt-get -y autoremove && apt-get autoclean
                    ;;
                esac
                rm -rf /tmp/* ~/.bash_history
            }

            dismantle_chroot()
            {
                # Dismantle chroot environment
                rm /var/lib/dbus/machine-id
                rm /sbin/initctl
                dpkg-divert --rename --remove /sbin/initctl
                umount /proc || umount -lf /proc
                umount /sys
                umount /dev/pts
            }
 
            # Logging and error handling functions
            log() { printf '%s\n' "$*"; }
            error() { log "ERROR: $*" >&2; }
            fatal() { error "$*"; exit 1; }
            chroot_fatal() { error "$*"; dismantle_chroot >&2; exit 1; }

            # Catch Ctrl+C keyboard command to cleanly exit from chroot  
            trap control_c SIGINT

            control_c()
            # Run if user hits control-c
            {
                echo -en "\n*** Caught Ctrl+C! Exiting cleanly... ***\n"
                dismantle_chroot
                echo "Done!"
                exit $?
            }

            setup_repos()
            {
                mkdir -p "$addonsdir"
                mkdir -p "$addonsoca"
                mkdir -p "$addonscustom"

                find "$odoodir" -type d -exec chmod 750 {} \;
                chown -R $user:$user $addonsdir

                # New in git >= 2.27.0 https://salferrarello.com/git-warning-pulling-without-specifying-how-to-reconcile-divergent-branches-is-discouraged/
                [[ ${release} == "bullseye" ]] && (git config --global pull.ff only)

                if [ ! -d "$odoodir/$odoover.0" ]
                then
                    git clone https://github.com/OCA/OCB.git --depth=1 --branch=$odoover.0 --single-branch $odoodir/$odoover.0 || exit 1
                else
                    git -C $odoodir/$odoover.0 pull origin $odoover.0
                fi

                chown -R $user:$user $odoodir/$odoover.0

                for repo in $ocarepos
                do
                    if [ ! -d "$addonsoca/$repo" ]
                    then
                        git clone https://github.com/OCA/$repo.git --depth=1 --branch=$odoover.0 --single-branch $addonsoca/$repo || exit 1
                    else
                        git -C $addonsoca/$repo pull origin $odoover.0
                    fi
                done

                for repo in $extrarepos
                do
                    if [ ! -d "$addonscustom/$repo" ]
                    then
                        if [[ ${repo} == "iso_addons" ]]
                        then
                            git clone https://gitlab.com/PNLUG/Odoo/repository/$repo.git --depth=1 --branch=$odoover.0 --single-branch $addonscustom/$repo || exit 1
                        else
                            if [[ ${odoover} == "12" ]]
                            then
                                git clone https://github.com/muk-it/$repo.git --depth=1 --branch=$odoover.0 --single-branch $addonscustom/$repo || exit 1
                            fi
                        fi
                    else
                        git -C $addonscustom/$repo pull origin $odoover.0
                    fi
                done

                # Setup modules to install
                modules=""

                for repo in ${repos2install}
                do
                    modules+="`ls -l ${addonsoca}/${repo} | grep ^d | awk '{print $9}' | grep -v 'setup'`,"
                done
                if [[ ! -z "$extramodules" ]]
                then
                    for module in ${extramodules}
                    do
                        modules+="${module},"
                    done
                fi
                modules=${modules%,}
                modules=`echo ${modules} | sed 's/ /,/g'`
            }

            install_packages()
            {
                apt install -y $packages

                # Installing external packages
                cd /root

                case "$distribution" in
                ubuntu)
                    dpkg -l wkhtmltox >/dev/null 2>&1 || ((wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.${release}_amd64.deb || return 1) && dpkg -i wkhtmltox_0.12.5-1.${release}_amd64.deb && cp -p /usr/local/bin/wkhtmlto* /usr/bin/)
                    ;;
                debian)
                    dpkg -l wkhtmltox >/dev/null 2>&1 || ((wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.buster_amd64.deb || return 1) && dpkg -i wkhtmltox_0.12.5-1.buster_amd64.deb && cp -p /usr/local/bin/wkhtmlto* /usr/bin/)
                    ;;
                esac

                # Clean up root dir
                [[ $(ls -A | grep ".deb" | wc -l) -ne 0 ]] && rm *.deb

                return 0
            }

            setup_chroot

            # Check actual ISO release
            release=`lsb_release -c | cut -f 2`
            distribution=`lsb_release -i | cut -f 2 | awk '{print tolower($0)}'`
            locale="it_IT.UTF-8" 

            # Set Odoo release
            [[ ${release} == "bionic" || ${release} == "buster" ]] && odoover="12"
            [[ ${release} == "focal" || ${release} == "bullseye" ]] && odoover="14"

            # Set locale
            sed -i "s/\(.*\)\($locale\)/\2/" /etc/locale.gen
            locale-gen $locale
            update-locale LANG=$locale LANGUAGE=$locale LC_ALL=$locale

            # Setup packages for corresponding ISO
            [[ ${release} == "bionic" ]] && packages="postgresql git xfonts-base xfonts-75dpi build-essential libc-dev libxslt-dev libzip-dev libldap2-dev libsasl2-dev python3-pip python3-setuptools python3-venv python3-dev"
            [[ ${release} == "focal" ]] && packages="postgresql git xfonts-base xfonts-75dpi build-essential libc-dev libxslt-dev libzip-dev libldap2-dev libsasl2-dev libpq-dev python3-pip python3-setuptools python3-venv python3-dev"
            [[ ${distribution} == "debian" ]] && packages="postgresql git wget xfonts-base build-essential libffi-dev libpq-dev libxslt-dev libzip-dev libldap2-dev libsasl2-dev python3-pip python3-setuptools python3-venv python3-dev"

            # Setup Odoo variables
            user="odoo"
            odoodir="/opt/$user"
            addonsdir="$odoodir/addons$odoover"
            addonsoca="$addonsdir/OCA"
            addonscustom="$addonsdir/custom"

            ocarepos="
                      l10n-italy
                      account-analytic
                      account-budgeting
                      account-closing
                      account-consolidation
                      account-financial-tools
                      account-financial-reporting
                      account-fiscal-rule
                      account-invoicing
                      account-invoice-reporting
                      account-payment
                      account-reconcile
                      bank-statement-import
                      bank-payment
                      brand
                      business-requirement
                      calendar
                      commission
                      community-data-files
                      connector
                      connector-cmis
                      connector-ecommerce
                      connector-interfaces
                      connector-jira
                      connector-magento
                      connector-prestashop
                      connector-telephony
                      contract
                      credit-control
                      crm
                      currency
                      data-protection
                      ddmrp
                      delivery-carrier
                      dms
                      donation
                      e-commerce
                      edi
                      event
                      field-service
                      fleet
                      geospatial
                      helpdesk
                      hr
                      interface-github
                      intrastat-extrastat
                      iot
                      knowledge
                      maintenance
                      management-system
                      manufacture
                      manufacture-reporting
                      margin-analysis
                      mis-builder
                      mis-builder-contrib
                      multi-company
                      odoo-pim
                      operating-unit
                      partner-contact
                      pos
                      product-attribute
                      product-pack
                      product-variant
                      project
                      project-agile
                      project-reporting
                      purchase-reporting
                      purchase-workflow
                      queue
                      report-print-send
                      reporting-engine
                      rest-framework
                      rma
                      sale-reporting
                      sale-workflow
                      search-engine
                      server-auth
                      server-backend
                      server-brand
                      server-tools
                      server-ux
                      social
                      stock-logistics-barcode
                      stock-logistics-reporting
                      stock-logistics-tracking
                      stock-logistics-transport
                      stock-logistics-warehouse
                      stock-logistics-workflow
                      storage
                      survey
                      timesheet
                      vertical-abbey
                      vertical-agriculture
                      vertical-association
                      vertical-hotel
                      vertical-isp
                      vertical-rental
                      vertical-travel
                      web
                      website
                      website-cms
                      website-themes
                      wms
                      "

            repos2install="l10n-italy"

            [[ ${odoover} == "12" ]] && extrarepos="iso_addons muk_base muk_web" && extramodules="l10n_it full_accounting_activation muk_web_theme auto_backup disable_odoo_online portal_odoo_debranding remove_odoo_enterprise"
            [[ ${odoover} == "14" ]] && ocarepos+="hr-attendance hr-expense hr-holidays payroll pms product-configurator sale-promotion vertical-realestate" && extrarepos="iso_addons" && extramodules="l10n_it full_accounting_activation web_responsive auto_backup disable_odoo_online portal_odoo_debranding remove_odoo_enterprise"

            database="contabita"

            # Setup repos directories
            repodirs="$odoodir/$odoover.0/addons,$odoodir/$odoover.0/odoo/addons,"
            for repo in ${ocarepos}
            do
                repodirs+="${addonsoca}/${repo},"
            done
            if [[ ! -z "$extrarepos" ]]
            then
                for repo in ${extrarepos}
                do
                    repodirs+="${addonscustom}/${repo},"
                done
            fi
            repodirs=${repodirs%,}

            # Add Ubuntu universe repository
            if [[ ${distribution} == "ubuntu" ]]
            then
                [[ -z $(cat /etc/apt/sources.list | grep universe) ]] && echo "deb http://archive.ubuntu.com/ubuntu ${release} universe" >> /etc/apt/sources.list && echo "deb http://archive.ubuntu.com/ubuntu ${release}-updates universe" >> /etc/apt/sources.list
            fi

            # Update ISO
            apt update || chroot_fatal "Cannot install updates. Check your connection and try again."

            # Installing needed packages

            install_packages

            if [ $? -eq 0 ]
            then
                service postgresql start || chroot_fatal "Cannot start postgresql service. Exiting."

                ## Create odoo user
                su - postgres -c "psql -t -c '\du' | cut -d \| -f 1 | grep -qw $user" && echo -e "Found postgres user $user." || su - postgres -c "createuser -s $user"
                id "$user"&>/dev/null && echo -e "Found user $user." || adduser --system --no-create-home --disabled-login --group $user
                usermod -s /bin/bash -d $odoodir $user

                ## Setup Odoo addons

                setup_repos || chroot_fatal "Cannot setup required repositories. Check your connection and try again"

                ## Enable bash and setup prompt for odoo user
                mv ./profile $odoodir/.profile
                mv ./bashrc $odoodir/.bashrc

                ## Setup OCB/OCA modules requirements
                python3 -m venv $odoodir/venv$odoover
                chown -R $user:$user $odoodir
                source $odoodir/venv$odoover/bin/activate
                [[ ${release} == "bionic" || ${release} == "buster" ]] && (pip install --upgrade pip || chroot_fatal "Cannot upgrade pip in virtualenv. Check your connection and try again")
                pip install wheel
                pip3 install pypdf phonenumbers asn1crypto codicefiscale unidecode pysftp -r $odoodir/$odoover.0/requirements.txt || chroot_fatal "Cannot install Odoo python packages in virtualenv. Check your connection and try again"
                [[ ${odoover} == "12" ]] && (pip3 install pyxb==1.2.6 || chroot_fatal "Cannot install pyxb 1.2.6 in virtualenv. Check your connection and try again")
		[[ ${odoover} == "14" ]] && (pip3 install pyPDF2 cryptography -r $addonsoca/$repos2install/requirements.txt || chroot_fatal "Cannot install $repos2install requirements in virtualenv. Check your connection and try again")

                ## Setup odoo config and log file
                mkdir -p /etc/odoo
                [[ ! -f "/etc/odoo/odoo$odoover.conf" ]] && cp /opt/$user/$odoover.0/debian/odoo.conf /etc/odoo/odoo$odoover.conf
                chmod 640 /etc/odoo/odoo$odoover.conf
                chown $user:$user /etc/odoo/odoo$odoover.conf

                mkdir -p /var/log/odoo
                chown root:$user /var/log/odoo && chmod 750 /var/log/odoo
                touch /var/log/odoo/odoo$odoover-server.log
                chown $user:adm /var/log/odoo/odoo$odoover-server.log && chmod 640 /var/log/odoo/odoo$odoover-server.log

                ## Create and setup database
                su - odoo -c "psql -lqt | cut -d \| -f 1 | grep -qw $database"

                if [ $? -eq 0 ]
                then
                    echo -e "Found database $database."
                else
                    chown -R $user:$user ${addonsoca} ${addonscustom}
                    echo "addons_path = ${repodirs}" >> /etc/odoo/odoo$odoover.conf

                    ## Create database without demo data
                    su - odoo -c "createdb ${database} -O ${user}"

                    ## Create database with demo data
                    su - odoo -c "createdb ${database}_demo -O ${user}"

                    su - odoo -c "source $odoodir/venv$odoover/bin/activate && $odoodir/$odoover.0/odoo-bin -c /etc/odoo/odoo$odoover.conf -i base,${modules} -d ${database} --without-demo=all --load-language=it_IT --stop-after-init"

                    su - odoo -c "source $odoodir/venv$odoover/bin/activate && $odoodir/$odoover.0/odoo-bin -c /etc/odoo/odoo$odoover.conf -i base,${modules} -d ${database}_demo --load-language=it_IT --stop-after-init"

                    ## Kill process if odoo doesn't stop after initialization
                    if [ $(pgrep -x -c odoo) -gt 0 ]
                    then
                        echo -e "Odoo still running... Force stop. "
                        killall odoo
                    fi
                fi

                # Enable odoo systemd service
                systemctl enable odoo$odoover.service

                service postgresql stop
            else
                chroot_fatal "Cannot install required packages. Check URL and your connection and try again"
            fi

            # Clean up root dir
            [[ $(ls -A /root | wc -l) -eq 0 ]] && rm -r *

            # Setup launcher & icon
            [[ ${distribution} == "ubuntu" ]] && echo 'pref("datareporting.policy.firstRunURL", "");' >> /etc/firefox/syspref.js
            [[ ${distribution} == "debian" ]] && echo 'pref("datareporting.policy.firstRunURL", "");' >> /etc/firefox-esr/firefox-esr.js
            chmod 644 /usr/share/applications/odoo_ita.desktop
            chmod 644 /usr/share/icons/odoo_pnlug-128x128.png
            update-desktop-database

            chmod 644 /usr/share/glib-2.0/schemas/99_odoo-settings.gschema.override
            glib-compile-schemas /usr/share/glib-2.0/schemas/

            # Set locale
            dpkg-reconfigure locales

            # Set keyboard layout
            dpkg-reconfigure keyboard-configuration

            clean_chroot
            dismantle_chroot
            echo "Done!"
EOF
            sudo umount -l $editdir/dev || exit 1

            break
            ;;

        # Create ISO
        3 ) [ ! -d "$editdir" ] && error "Environment is not set. Please select option 1) first." && break
            sudo cp ./files/splash.png $extractdir/isolinux/

            sudo cp ./files/stdmenu-$distro.cfg $extractdir/isolinux/stdmenu.cfg
            sudo chmod 444 $extractdir/isolinux/stdmenu.cfg

            sudo cp ./files/menu-$distro.cfg $extractdir/isolinux/menu.cfg
            sudo chmod 444  $extractdir/isolinux/menu.cfg
            if [[ $distro == "ubuntu" ]]
            then
                sudo cp ./files/loopback-$distrorel.cfg $extractdir/boot/grub/loopback.cfg
                sudo chmod 444 $extractdir/boot/grub/loopback.cfg
	        sudo sed -i "s/<odoo_release>/$odoorel/g" $extractdir/boot/grub/loopback.cfg

                sudo cp ./files/isolinux_txt-$distrorel.cfg $extractdir/isolinux/txt.cfg
                sudo chmod 444 $extractdir/isolinux/txt.cfg

                sudo chmod +w $extractdir/$fsdir/filesystem.$fsext
                sudo chroot $editdir dpkg-query -W --showformat='${Package} ${Version}\n' | sudo tee $extractdir/$fsdir/filesystem.$fsext

                sudo cp $extractdir/$fsdir/filesystem.$fsext $extractdir/$fsdir/filesystem.$fsext"-desktop"

                sudo sed -i '/ubiquity/d' $extractdir/$fsdir/filesystem.$fsext"-desktop"

                sudo sed -i '/${fsdir}/d' $extractdir/$fsdir/filesystem.$fsext"-desktop"

                [[ -f $extractdir/$fsdir/filesystem.squashfs ]] && sudo rm $extractdir/$fsdir/filesystem.squashfs
                [[ -f $destdir/$customiso ]] && sudo rm $destdir/$customiso

                [[ -L $editdir/etc/systemd/system/graphical.target.wants/ubiquity.service ]] && sudo rm $editdir/etc/systemd/system/graphical.target.wants/ubiquity.service

                sudo rm $extractdir/md5sum.txt
            fi
	    if [[ $distro == "debian" ]]
            then
                sudo sed -i "s/<odoo_release>/$odoorel/g" $extractdir/isolinux/menu.cfg

                kernelver=`sudo chroot $editdir dpkg-query -W -f='${binary:Package}\n' linux-image-* | head -n 1 | sed 's/linux-image-//'`
		sudo sed -i "s/<kernel_version>/$kernelver/g" $extractdir/isolinux/menu.cfg
	    fi

            sudo mksquashfs $editdir $extractdir/$fsdir/filesystem.squashfs -b 1048576

            printf $(sudo du -sx --block-size=1 $editdir | cut -f1) | sudo tee $extractdir/$fsdir/filesystem.size

            cd $extractdir
            find -type f -print0 | sudo xargs -0 md5sum | grep -v isolinux/boot.cat | sudo tee md5sum.txt
            sudo genisoimage -D -r -V "$IMAGE_NAME" -cache-inodes -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o $destdir/$customiso .
            sudo chown $user:$user $destdir/$customiso
            echo -e "\nDONE!\nYou can find your customized ISO $customiso in $destdir.\n"
            break 2
            ;;

        # Reset environment
        4 ) echo -ne "\n"
            [[ ! -d "$releasedir" ]] && error "Environment not found, nothing to wipe. Exiting." && break
            if mount | grep $mntdir > /dev/null;
            then
                sudo umount $mntdir
            fi
            sudo rm -r $releasedir && echo -ne "Done, environment has been wiped.\n\n"
            break
            ;;

        $(( ${#options[@]}+1 )) ) echo -ne "\nGoodbye!\n"
            break 2
            ;;

        *)  echo "Invalid option. Please try another one."
            continue
            ;;  
        esac
    done
done
